resource "helm_release" "alb_ingress_controller" {
  name       = "aws-load-balancer-controller"
  repository = "https://aws.github.io/eks-charts"
  chart      = "aws-load-balancer-controller"
  version    = "1.7.2"
  namespace = "kube-system"
  create_namespace = true

  set {
    name  = "serviceAccount.annotations.eks\\.amazonaws\\.com/role-arn"
    value = aws_iam_role.this.arn
  }

  set {
    name  = "clusterName"
    value = module.eks.cluster_name
  }

}

resource "helm_release" "elasticsearch" {
  name       = "elasticsearch"
  repository = "https://helm.elastic.co"
  chart      = "elasticsearch"
  version    = "8.5.1"
  namespace = "monitoring"
  create_namespace = true
}

resource "helm_release" "filebeat" {
  name       = "filebeat"
  repository = "https://helm.elastic.co"
  chart      = "filebeat"
  version    = "8.5.1"
  namespace = "monitoring"
  create_namespace = true

  values = [
    "${file("settings/helm/filebeat.values.yaml")}"
  ]
}

resource "helm_release" "prometheus" {
  name       = "prometheus"
  repository = "https://prometheus-community.github.io/helm-charts"
  chart      = "prometheus"
  version    = "25.19.1"
  namespace = "monitoring"
  create_namespace = true

  values = [
    "${file("settings/helm/prometheus.values.yaml")}"
  ]
}

resource "helm_release" "kubernetes_dashboard" {
  name       = "kubernetes-dashboard"
  repository = "https://kubernetes.github.io/dashboard"
  chart      = "kubernetes-dashboard"
  version    = "7.1.3"
  namespace = "monitoring"
  create_namespace = true

  values = [
    "${file("settings/helm/kubernetes-dashboard.values.yaml")}"
  ]
}