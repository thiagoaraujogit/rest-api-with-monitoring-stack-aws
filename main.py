import logging, os, json, boto3, secrets, sys
from botocore.exceptions import ClientError
from fastapi import FastAPI, status
from boto3.dynamodb.types import TypeDeserializer, TypeSerializer
from fastapi.responses import JSONResponse
from prometheus_fastapi_instrumentator import Instrumentator

FORMAT = '{"time": "%(asctime)s", "level": "%(levelname)s", "message": "%(message)s"}'

logging.basicConfig(level=logging.INFO, format=FORMAT)
logger = logging.getLogger(__name__) 

DYNAMODB_TABLE = os.getenv('DYNAMODB_TABLE')
AWS_REGION = os.getenv('AWS_REGION')

if DYNAMODB_TABLE is None:
    logging.error('Missing variable DYNAMODB_TABLE')
    sys.exit(1)
if AWS_REGION is None:
    logging.error('Missing variable AWS_REGION')
    sys.exit(1)


dynamodb = boto3.resource('dynamodb', region_name=AWS_REGION)
db_table = dynamodb.Table(DYNAMODB_TABLE)

app = FastAPI(
    debug=False,
    title="rest-api-with-monitoring-stack-aws",
    version="1.0.0",
    description="This is a REST API sample written in Python to study observability and monitoring stacks."
)

Instrumentator().instrument(app).expose(app)

@app.get("/")
def read_root():
    return {"Welcome to the REST API With Monitoring Stack AWS project! :)"}

@app.get("/health")
def health_check():
    return {"status": "ok"}

@app.get("/unicorn")
def create_unicorn(code: int):
    unicorn_hash = secrets.token_hex(nbytes=16)
    try:
        db_table.put_item(
            Item={
                'unicorn_code': f'{code}',
                'unicorn_hash': f'{unicorn_hash}'
            }
        )
        logging.info(f'New unicorn registered! Hash: {unicorn_hash}')
        return {"message": "Unicorn registered, congratulations!"}
    except ClientError as error:
        logging.error('Error trying to create unicorn on database, caused by: ', error)

@app.get("/unicorn/{unicorn_code}")
def get_unicorn(unicorn_code: str):
    try:
        unicorn_result = db_table.get_item(
            Key={
                'unicorn_code': str(unicorn_code)
            }
        )
        if 'Item' in unicorn_result: 
            return unicorn_result['Item']
        else:
            return JSONResponse(status_code=status.HTTP_404_NOT_FOUND, content={"error": "Unicorn not found in database"})
    except ClientError as error:
        logging.error('Error trying to get unicorn on database, caused by: ', error)
